from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from django.contrib.auth.models import User

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {"form": form}
        return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    user = User.objects.get(id=request.user.id)
    tasks = user.tasks.all()
    context = {"tasks": tasks}
    print(request.user.tasks)
    return render(request, "tasks/mine.html", context)
