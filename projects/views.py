from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm
from tasks.forms import TaskForm

# Create your views here.


@login_required
def project_list(request):
    list = Project.objects.filter(owner=request.user)
    context = {"lists": list}

    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    has_tasks = False
    project = Project.objects.get(id=id)
    if len(project.tasks.all()) > 0:
        has_tasks = True
    context = {
        "project": project,
        "has_tasks": has_tasks,
    }
    return render(
        request,
        "projects/detail.html",
        context,
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")

    else:
        form = ProjectForm()
        context = {"form": form}
        return render(request, "projects/create.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {"form": form}
        return render(request, "tasks/create.html", context)
