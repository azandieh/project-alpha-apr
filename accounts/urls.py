from django.urls import path
from .views import login_user, user_logout, user_signup

urlpatterns = [
    path("signup/", user_signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("login/", login_user, name="login"),
]
